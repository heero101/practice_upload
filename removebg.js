"use strict";

const { Readable } = require("stream");
const Jimp = require("jimp");
const config = require("./config");

async function handleRemoveBg(ctx, next) {
  if(config.allowed_processing.background_removal && ctx.img_processing.options.transparent_background) {
      const processedImageStream = await Jimp.read(ctx.img_processing.file).then(async (image) => {        
        if(!imageMimeTypes.includes(image.getMIME())) {
          throw new Error('File type not allowed for background color removal')
        }

        const transparentImage = removeBgColor(image);

        const croppedImage = autoCrop(transparentImage);

        return await bufferToStream(
          await croppedImage.getBufferAsync(Jimp.MIME_PNG)
        );
      });   
      const tempPath = ctx.img_processing.file.path
      ctx.img_processing.file = processedImageStream
      ctx.img_processing.file.path = tempPath
  }

  return await next();
}

async function handleRemoveBg2(ctx, next) {
  console.log(
    'naninla3',
    {ctx}
  )

  ctx.img_processing.file = 'hello'
 
  await next();
}

const imageMimeTypes = ['image/jpg', 'image/jpeg', 'image/png']

function removeBgColor(image) {  
  image
    .greyscale() // set greyscale
    .contrast(1); // set contrast to maximum so that it can be black and white

  const targetColor = { r: 255, g: 255, b: 255, a: 255 }; // Color you want to replace
  const replaceColor = { r: 0, g: 0, b: 0, a: 0 }; // Color you want to replace with
  const colorDistance = (c1, c2) => {
    return Math.sqrt(
      Math.pow(c1.r - c2.r, 2) +
        Math.pow(c1.g - c2.g, 2) +
        Math.pow(c1.b - c2.b, 2) +
        Math.pow(c1.a - c2.a, 2)
    ); // Distance between two colors
  };
  const threshold = 32; // Replace colors under this threshold. The smaller the number, the more specific it is.
  // loop trough the bits
  image.scan(0, 0, image.bitmap.width, image.bitmap.height, (x, y, idx) => {
    const thisColor = {
      r: image.bitmap.data[idx + 0],
      g: image.bitmap.data[idx + 1],
      b: image.bitmap.data[idx + 2],
      a: image.bitmap.data[idx + 3],
    };
    // replace bit's color to "transparent" or "0" if its lower or equal the threshold
    if (colorDistance(targetColor, thisColor) <= threshold) {
      image.bitmap.data[idx + 0] = replaceColor.r;
      image.bitmap.data[idx + 1] = replaceColor.g;
      image.bitmap.data[idx + 2] = replaceColor.b;
      image.bitmap.data[idx + 3] = replaceColor.a;
    }
  });

  return image;
}

function autoCrop(image) {
  image.autocrop();

  return image;
}

/**
 * @param binary Buffer
 * returns readableInstanceStream Readable
 */
function bufferToStream(binary) {
  const buffer = binary;
  const readable = new Readable();
  readable._read = () => {}; // _read is required but you can noop it
  readable.push(buffer);
  readable.push(null);

  return readable;
}


module.exports = {
    handleRemoveBg,
    handleRemoveBg2,
    bufferToStream
}
