const koa = require('koa');
const Router = require('koa-router');
const {koaBody: bodyParser} = require('koa-body');
const fs = require('fs')
const { upload } = require('./upload');
const { handleRemoveBg, bufferToStream } = require("./removebg")

const app = new koa();
const router = new Router();

app.use(bodyParser({
    multipart: true
}));

// get method route
router.post('/hello', async(ctx) => {    
    let data = ''
    if (!ctx.request.is('multipart/*')) {
        ctx.status = 200;
        ctx.body = 'no file';
    }
    const buffer = fs.readFileSync(ctx.request.files.file.filepath) // this is only for this case since I'm using a different body parser    

    await upload(__dirname, buffer, ctx, parseOptions(ctx.request.body.options))    
    
    ctx.status = 200;
    ctx.body = ctx.request.body;
});

router.get('/hello', async(ctx) => {
  const data =  await readFileThunk(__dirname + '/index.html')
        console.log('yasss')
  ctx.status = 200;
  ctx.body = data;
});

app.use(router.routes());
app.listen(8081);


function readFileThunk(src) {
    return new Promise(function (resolve, reject) {
        fs.readFile(src, {'encoding': 'utf8'}, function (err, data) {
        if(err) return reject(err);
        resolve(data);
        });
    });
}


function parseOptions(optionsString) {
    const options = optionsString.split(',')
    const optionOjb = {}
    for (const option of options) {
        optionOjb[option] = true
    }

    return optionOjb
}
  