const { imageProcessing } = require("./imageProcessing")
const { handleRemoveBg, bufferToStream } = require("./removebg")
const fs = require('fs')
const crypto = require('crypto')
const base64 = require('base64url')
const  path = require('path')
const config = require("./config")

const fileSizeLimit = 3500000 // file size limit in bytes

async function upload(dir, stream, ctx, {...options}) {    
    ctx.img_processing = {
        options: {...options},
        file: stream
    }

    await imageProcessing(stream, {...options})(ctx)
    
    if(!config.allowed_processing.background_removal) {
        stream = bufferToStream(ctx.img_processing.file) // have to add this due to the body parsing package I used
    } else {
        stream = ctx.img_processing.file
    }

	return new Promise((resolve, reject) => {
        const id = `${makeID()}.png`
        const output = fs.createWriteStream(path.join(dir, id))

		stream.pipe(output)

		output.on('error', reject)
		output.on('finish', () => {
			resolve({id})
		})
	})
}


module.exports = {
    upload
}

function hasReachedLimit(size) {
	return fileSizeLimit !== 0 && size > fileSizeLimit
}

function makeID() {
	return base64.encode(crypto.randomBytes(12))
}