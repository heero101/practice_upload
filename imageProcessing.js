const compose = require('koa-compose')
const config = require('./config')
const { handleRemoveBg, handleRemoveBg2 } = require('./removebg')

function imageProcessing(file, {...options}) {
	const middlewares = []
	if(config.allowed_processing.background_removal) {
		middlewares.push(handleRemoveBg)
	}
	// add all other processing conditions to control what processing is allowed

	const all = compose([...middlewares])

	return all
}

module.exports = {
    imageProcessing
}